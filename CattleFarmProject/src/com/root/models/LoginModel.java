package com.root.models;

import java.sql.*;

public class LoginModel {
    Connection connection;
    public LoginModel(){
        connection = SQLiteConnection.connector();

        if (connection == null) {
            System.out.println("Unable to connect To Database");
            System.exit(1);
        }
        else {
            System.out.println("Connected To database");
        }
    }
    /*public boolean isConnected(){
        try {
            return !connection.isClosed();
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }*/


    public boolean isLogin(String premiseId) throws SQLException {

        connection = SQLiteConnection.connector();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String query = "select * from Users Where PremiseId = ?";

        if (connection == null) {
            System.out.println("Unable to connect To Database");
            System.exit(1);
        }
        else {
            System.out.println("Connected To database");
        }

        try {

            //System.out.println("First to Database Query");
            preparedStatement = connection.prepareStatement(query);
            //System.out.println("Prepared Statement");

            preparedStatement.setString(1,premiseId);

            resultSet = preparedStatement.executeQuery();

            //System.out.println(resultSet);
            return resultSet.next();

        }
        catch (Exception e){
            System.out.println("Into Exception Block");
            return false;
        }
        finally {

            preparedStatement.close();
            resultSet.close();
        }
    }


}
