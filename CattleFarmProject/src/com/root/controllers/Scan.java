package com.root.controllers;

import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;


public class Scan implements Initializable {


    @FXML
    private AnchorPane scanRootAnchorPane;

    @FXML
    private Label scanTm, scancmt;
    @FXML
    private JFXTextField mnPremiseId;


//    InputStream reader;
//    Properties p;
//
//    {
//        try {
//            reader = Scan.class.getResourceAsStream("string.properties");
//            p = new Properties();
//            p.load(reader);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }


    @FXML
    public void backButtonAction(ActionEvent event) throws IOException {
        Home.checkboxStatus = false;
        AnchorPane pane = FXMLLoader.load(getClass().getResource("../scenes/home.fxml"));
        scanRootAnchorPane.getChildren().setAll(pane);
        //event.consume();

    }


    @Override
    public void initialize (URL location, ResourceBundle resources) {


        boolean checkBoxStatus = Home.checkboxStatus;

        String mainPremiseId = mnPremiseId.getText();


        String scanPremiseId = mainPremiseId.split("-")[0];
        System.out.println(scanPremiseId);
        String PremiseId = "002345A";

        if (scanPremiseId.equals(PremiseId)) {
            System.out.println("The two strings are the same.");
            scancmt.setText("The two strings are the same.");
            System.out.println("Validation :"+checkBoxStatus);
            if(checkBoxStatus) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../scenes/weightPopUp.fxml"));
                Parent root1 = null;
                try {
                    root1 = fxmlLoader.load();
                    Stage stage = new Stage();
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.initStyle(StageStyle.UNDECORATED);
                    stage.setTitle("ABC");
                    stage.setScene(new Scene(root1));
                    stage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else {
            System.out.println("The two strings are not the same.");
            scancmt.setText("The two strings are not the same.");

        }

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));

        Calendar cal = Calendar.getInstance();

        scanTm.setText(dateFormat.format(date));

    }


}
