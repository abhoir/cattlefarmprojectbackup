package com.root.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import com.root.models.LoginModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class Login implements Initializable {

    String red = "#ee1313";
    String green = "12ed25";

    @FXML
    private JFXButton loginOkButton;

    @FXML
    private JFXTextField loginTextField;

    @FXML
    private Label loginValidationLable;

    @FXML
    private AnchorPane loginRootAnchorPane;


    public void initialize() {
//        thirdAnchorPane.widthProperty().bind(mainAnchorPane.widthProperty());
//        thirdAnchorPane.widthProperty().
    }




    @FXML
    void loginButtonClick(ActionEvent event) throws IOException {

        String str = loginTextField.getText();
        if(str.isEmpty()){
            loginValidationLable.setTextFill(Paint.valueOf(red));
            loginValidationLable.setText("Please enter the Premise ID");

        }
        else if(str.length()!= 7){
            loginValidationLable.setTextFill(Paint.valueOf(red));
            loginValidationLable.setText("Please enter Valid Premise ID");
        }
        else {
            //Database Query
            System.out.println(str);


            //Save Premise ID somwhere permenantly
            AnchorPane pane = FXMLLoader.load(getClass().getResource("../scenes/home.fxml"));
            loginRootAnchorPane.getChildren().setAll(pane);




            /*try {
                if(loginModel.isLogin(str)){
                    loginValidationLable.setTextFill(Paint.valueOf(green));
                    loginValidationLable.setText("Authenticated User");

                }
                else {
                    loginValidationLable.setTextFill(Paint.valueOf(red));
                    loginValidationLable.setText("Unauthorised User");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }*/




        }
        event.consume();
    }








    @FXML
    void loginTextTyped(KeyEvent event) {

        if(loginValidationLable.getText()!= null){
            loginValidationLable.setText(null);
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loginOkButton.setDefaultButton(true);
    }
}

