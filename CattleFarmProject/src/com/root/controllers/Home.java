package com.root.controllers;

import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class Home implements Initializable {

    @FXML
    private AnchorPane homeRootAnchorPane;

    public static boolean checkboxStatus = false;


    @FXML
    private ImageView startScanIcon;

    @FXML
    private ImageView uploadScanIcon;


    //boolean checkboxStatus = false;

//    InputStream reader;
//    Properties p;
//
//    {
//        try {
//            reader = Home.class.getResourceAsStream("string.properties");
//            p=new Properties();
//            p.load(reader);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    @FXML
    void promptCheckboxAction(ActionEvent event) {
        checkboxStatus = !checkboxStatus;
        //p.setProperty("checkBoxStatus", String.valueOf(checkboxStatus));
        System.out.println(checkboxStatus);
        //System.out.println(p.getProperty("checkBoxStatus"));
        //event.consume();
    }


    @FXML
    public void scanButtonAction(ActionEvent event) throws IOException {

        AnchorPane pane = FXMLLoader.load(getClass().getResource("../scenes/scan.fxml"));
        homeRootAnchorPane.getChildren().setAll(pane);
        //event.consume();

    }




    @FXML
    public void uploadButtonAction(ActionEvent evt) throws IOException {


        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../scenes/flushPop.fxml"));
        Parent root = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setTitle("Uploading");
        stage.setScene(new Scene(root));
        stage.show();

        PauseTransition delay = new PauseTransition(Duration.seconds(5));
        delay.setOnFinished(event -> stage.close());
        delay.play();
        //evt.consume();

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        startScanIcon.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                System.out.println("Start Scan Pressed ");
                AnchorPane pane = null;
                try {
                    pane = FXMLLoader.load(getClass().getResource("../scenes/scan.fxml"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                homeRootAnchorPane.getChildren().setAll(pane);
                //event.consume();
            }
        });

        uploadScanIcon.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>()  {
            @Override
            public void handle(MouseEvent mouseEvent) {
                System.out.println("Upload Icon Pressed");
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../scenes/flushPop.fxml"));
                Parent root = null;
                try {
                    root = (Parent) fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initStyle(StageStyle.UNDECORATED);
                stage.setTitle("Uploading");
                stage.setScene(new Scene(root));
                stage.show();

                PauseTransition delay = new PauseTransition(Duration.seconds(5));
                delay.setOnFinished(event -> stage.close());
                delay.play();
                //event.consume();
            }
        });
    }
}
