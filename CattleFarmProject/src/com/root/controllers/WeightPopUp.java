package com.root.controllers;

import com.jfoenix.controls.JFXTextField;
import javafx.animation.PauseTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class WeightPopUp implements Initializable {

    @FXML
    Button okButton,cnclButton;

    @FXML
    JFXTextField weight;
    @FXML
    Label errText;



    @FXML
    public void onButtonAction(ActionEvent e) {

        String scanWeight = weight.getText();


        if (weight.getText() == null || weight.getText().trim().isEmpty()) {

            errText.setText("Please enter value");

        } else {
            System.out.println(scanWeight);
            // get a handle to the stage
            Stage stage = (Stage) okButton.getScene().getWindow();
            // do what you have to do
            stage.close();
        }
        //e.consume();

    }

    @FXML
    public void cnclButtonAction(ActionEvent et) {

        Stage stage = (Stage) cnclButton.getScene().getWindow();
        stage.close();
        //et.consume();

    }

    @FXML
    void clearWarning(KeyEvent event) {

        errText.setText(null);
        //event.consume();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


        weight.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    weight.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        okButton.setDefaultButton(true);
    }
}

